{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# InfoGAN report"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The latent space can be explored and generated images compared in an attempt to understand the mapping that the generator model has learned. Alternatively, the generation process can be conditioned, such as via a class label, so that images of a specific type can be created on demand. This is the basis for the Conditional Generative Adversarial Network, CGAN or cGAN for short.\n",
    "\n",
    "Another approach is to provide control variables as input to the generator, along with the point in latent space (noise). The generator can be trained to use the control variables to influence specific properties of the generated images. This is the approach taken with the Information Maximizing Generative Adversarial Network, or InfoGAN for short."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The structured mapping learned by the generator during the training process is somewhat random. Although the generator model learns to spatially separate properties of generated images in the latent space, there is no control. The properties are entangled. The InfoGAN is motivated by the desire to disentangle the properties of generated images.\n",
    "\n",
    "For example, in the case of faces, the properties of generating a face can be disentangled and controlled, such as the shape of the face, hair color, hairstyle, and so on."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mutual information refers to the amount of information learned about one variable given another variable. In this case, we are interested in the information about the control variables given the image generated using noise and the control variables.\n",
    "\n",
    "In information theory, mutual information between X and Y , I(X; Y ), measures the “amount of information” learned from knowledge of random variable Y about the other random variable X.\n",
    "\n",
    "The Mutual Information (MI) is calculated as the conditional entropy of the control variables (c) given the image (created by the generator (G) from the noise (z) and the control variable (c))  subtracted from the marginal entropy of the control variables (c); for example:\n",
    "\n",
    "MI = Entropy(c) – Entropy(c ; G(z,c))\n",
    "\n",
    "\n",
    "Calculating the true mutual information, in practice, is often intractable, although simplifications are adopted in the paper, referred to as Variational Information Maximization, and the entropy for the control codes is kept constant."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Training the generator via mutual information is achieved through the use of a new model, referred to as Q or the auxiliary model. The new model shares all of the same weights as the discriminator model for interpreting an input image, but unlike the discriminator model that predicts whether the image is real or fake, the auxiliary model predicts the control codes that were used to generate the image.\n",
    "\n",
    "Both models are used to update the generator model, first to improve the likelihood of generating images that fool the discriminator model, and second to improve the mutual information between the control codes used to generate an image and the auxiliary model’s prediction of the control codes.\n",
    "\n",
    "The result is that the generator model is regularized via mutual information loss such that the control codes capture salient properties of the generated images and, in turn, can be used to control the image generation process."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to implement infoGAN loss function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The InfoGAN is reasonably straightforward to implement once you are familiar with the inputs and outputs of the model.\n",
    "\n",
    "The only stumbling block might be the mutual information loss function, especially if you don’t have a strong math background, like most developers.\n",
    "\n",
    "There are two main types of control variables used with the InfoGan: categorical and continuous, and continuous variables may have different data distributions, which impact how the mutual loss is calculated. The mutual loss can be calculated and summed across all control variables based on the variable type, and this is the approach used in the official InfoGAN implementation released by OpenAI for TensorFlow.\n",
    "\n",
    "In Keras, it might be easier to simplify the control variables to categorical and either Gaussian or Uniform continuous variables and have separate outputs on the auxiliary model for each control variable type. This is so that a different loss function can be used, greatly simplifying the implementation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The categorical variable may be used to control the type or class of the generated image.\n",
    "\n",
    "This is implemented as a one hot encoded vector. That is, if the class has 10 values, then the control code would be one class, e.g. 6, and the categorical control vector input to the generator model would be a 10 element vector of all zero values with a one value for class 6, for example, [0, 0, 0, 0, 0, 0, 1, 0, 0].\n",
    "\n",
    "We do not need to choose the categorical control variables when training the model; instead, they are generated randomly, e.g. each selected with a uniform probability for each sample."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the auxiliary model, the output layer for the categorical variable would also be a one hot encoded vector to match the input control code, and the softmax activation function is used."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall that the mutual information is calculated as the conditional entropy from the control variable and the output of the auxiliary model subtracted from the entropy of the control variable provided to the input variable. We can implement this directly, but it’s not necessary.\n",
    "\n",
    "The entropy of the control variable is a constant and comes out to be a very small number close to zero; as such, we can remove it from our calculation. The conditional entropy can be calculated directly as the cross-entropy between the control variable input and the output from the auxiliary model. Therefore, the categorical cross-entropy loss function can be used, as we would on any multi-class classification problem.\n",
    "\n",
    "A hyperparameter, lambda, is used to scale the mutual information loss function and is set to 1, and therefore can be ignored."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The continuous control variables may be used to control the style of the image.\n",
    "\n",
    "The continuous variables are sampled from a uniform distribution, such as between -1 and 1, and provided as input to the generator model.\n",
    "\n",
    "The auxiliary model can implement the prediction of continuous control variables with a Gaussian distribution, where the output layer is configured to have one node, the mean, and one node for the standard deviation of the Gaussian, e.g. two outputs are required for each continuous control variable.\n",
    "\n",
    "The loss function must be calculated as the mutual information on the Gaussian control codes, meaning they must be reconstructed from the mean and standard deviation prior to calculating the loss. Calculating the entropy and conditional entropy for Gaussian distributed variables can be implemented directly, although is not necessary. Instead, the mean squared error loss can be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from numpy import zeros\n",
    "from numpy import ones\n",
    "from numpy import expand_dims\n",
    "from numpy import hstack\n",
    "from numpy.random import randn\n",
    "from numpy.random import randint\n",
    "from keras.datasets.mnist import load_data\n",
    "from keras.optimizers import Adam`\n",
    "from keras.initializers import RandomNormal\n",
    "from keras.utils import to_categorical\n",
    "from keras.models import Model\n",
    "from keras.layers import Input\n",
    "from keras.layers import Dense\n",
    "from keras.layers import Reshape\n",
    "from keras.layers import Flatten\n",
    "from keras.layers import Conv2D\n",
    "from keras.layers import Conv2DTranspose\n",
    "from keras.layers import LeakyReLU\n",
    "from keras.layers import BatchNormalization\n",
    "from keras.layers import Activation\n",
    "from matplotlib import pyplot\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "from tensorflow.python.keras import backend as K\n",
    "config = tf.compat.v1.ConfigProto( device_count = {'GPU': 1 , 'CPU': 4} ) \n",
    "sess = tf.compat.v1.Session(config=config) \n",
    "K.set_session(sess)\n",
    " \n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let’s start off by developing the generator model as a deep convolutional neural network (e.g. a DCGAN).\n",
    "\n",
    "The model could take the noise vector (z) and control vector (c) as separate inputs and concatenate them before using them as the basis for generating the image. Alternately, the vectors can be concatenated beforehand and provided to a single input layer in the model. The approaches are equivalent and we will use the latter in this case to keep the model simple.\n",
    "\n",
    "The define_generator() function below defines the generator model and takes the size of the input vector as an argument.\n",
    "\n",
    "A fully connected layer takes the input vector and produces a sufficient number of activations to create 512 7×7 feature maps from which the activations are reshaped. These then pass through a normal convolutional layer with 1×1 stride, then two subsequent upsamplings transpose convolutional layers with a 2×2 stride first to 14×14 feature maps then to the desired 1 channel 28×28 feature map output with pixel values in the range [-1,-1] via a tanh activation function.\n",
    "\n",
    "Good generator configuration heuristics are as follows, including a random Gaussian weight initialization, ReLU activations in the hidden layers, and use of batch normalization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define the standalone generator model\n",
    "def define_generator(gen_input_size):\n",
    "\t# weight initialization\n",
    "\tinit = RandomNormal(stddev=0.02)\n",
    "\t# image generator input\n",
    "\tin_lat = Input(shape=(gen_input_size,))\n",
    "\t# foundation for 7x7 image\n",
    "\tn_nodes = 512 * 7 * 7\n",
    "\tgen = Dense(n_nodes, kernel_initializer=init)(in_lat)\n",
    "\tgen = Activation('relu')(gen)\n",
    "\tgen = BatchNormalization()(gen)\n",
    "\tgen = Reshape((7, 7, 512))(gen)\n",
    "\t# normal\n",
    "\tgen = Conv2D(128, (4,4), padding='same', kernel_initializer=init)(gen)\n",
    "\tgen = Activation('relu')(gen)\n",
    "\tgen = BatchNormalization()(gen)\n",
    "\t# upsample to 14x14\n",
    "\tgen = Conv2DTranspose(64, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(gen)\n",
    "\tgen = Activation('relu')(gen)\n",
    "\tgen = BatchNormalization()(gen)\n",
    "\t# upsample to 28x28\n",
    "\tgen = Conv2DTranspose(1, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(gen)\n",
    "\t# tanh output\n",
    "\tout_layer = Activation('tanh')(gen)\n",
    "\t# define model\n",
    "\tmodel = Model(in_lat, out_layer)\n",
    "\treturn model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we can define the discriminator and auxiliary models.\n",
    "\n",
    "The discriminator model is trained in a standalone manner on real and fake images, as per a normal GAN. Neither the generator nor the auxiliary models are fit directly; instead, they are fit as part of a composite model.\n",
    "\n",
    "Both the discriminator and auxiliary models share the same input and feature extraction layers but differ in their output layers. Therefore, it makes sense to define them both at the same time.\n",
    "\n",
    "Again, there are many ways that this architecture could be implemented, but defining the discriminator and auxiliary models as separate models first allows us later to combine them into a larger GAN model directly via the functional API.\n",
    "\n",
    "The define_discriminator() function below defines the discriminator and auxiliary models and takes the cardinality of the categorical variable (e.g.number of values, such as 10) as an input. The shape of the input image is also parameterized as a function argument and set to the default value of the size of the MNIST images.\n",
    "\n",
    "The feature extraction layers involve two downsampling layers, used instead of pooling layers as a best practice. Also following best practice for DCGAN models, we use the LeakyReLU activation and batch normalization.\n",
    "\n",
    "The discriminator model (d) has a single output node and predicts the probability of an input image being real via the sigmoid activation function. The model is compiled as it will be used in a standalone way, optimizing the binary cross entropy function via the Adam version of stochastic gradient descent with best practice learning rate and momentum.\n",
    "\n",
    "The auxiliary model (q) has one node output for each value in the categorical variable and uses a softmax activation function. A fully connected layer is added between the feature extraction layers and the output layer, as was used in the InfoGAN paper. The model is not compiled as it is not for or used in a standalone manner."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define the standalone discriminator model\n",
    "def define_discriminator(n_cat, in_shape=(28,28,1)):\n",
    "\t# weight initialization\n",
    "\tinit = RandomNormal(stddev=0.02)\n",
    "\t# image input\n",
    "\tin_image = Input(shape=in_shape)\n",
    "\t# downsample to 14x14\n",
    "\td = Conv2D(64, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(in_image)\n",
    "\td = LeakyReLU(alpha=0.1)(d)\n",
    "\t# downsample to 7x7\n",
    "\td = Conv2D(128, (4,4), strides=(2,2), padding='same', kernel_initializer=init)(d)\n",
    "\td = LeakyReLU(alpha=0.1)(d)\n",
    "\td = BatchNormalization()(d)\n",
    "\t# normal\n",
    "\td = Conv2D(256, (4,4), padding='same', kernel_initializer=init)(d)\n",
    "\td = LeakyReLU(alpha=0.1)(d)\n",
    "\td = BatchNormalization()(d)\n",
    "\t# flatten feature maps\n",
    "\td = Flatten()(d)\n",
    "\t# real/fake output\n",
    "\tout_classifier = Dense(1, activation='sigmoid')(d)\n",
    "\t# define d model\n",
    "\td_model = Model(in_image, out_classifier)\n",
    "\t# compile d model\n",
    "\td_model.compile(loss='binary_crossentropy', optimizer=Adam(lr=0.0002, beta_1=0.5))\n",
    "\t# create q model layers\n",
    "\tq = Dense(128)(d)\n",
    "\tq = BatchNormalization()(q)\n",
    "\tq = LeakyReLU(alpha=0.1)(q)\n",
    "\t# q model output\n",
    "\tout_codes = Dense(n_cat, activation='softmax')(q)\n",
    "\t# define q model\n",
    "\tq_model = Model(in_image, out_codes)\n",
    "\treturn d_model, q_model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we can define the composite GAN model.\n",
    "\n",
    "This model uses all submodels and is the basis for training the weights of the generator model.\n",
    "\n",
    "The define_gan() function below implements this and defines and returns the model, taking the three submodels as input.\n",
    "\n",
    "The discriminator is trained in a standalone manner as mentioned, therefore all weights of the discriminator are set as not trainable (in this context only). The output of the generator model is connected to the input of the discriminator model, and to the input of the auxiliary model.\n",
    "\n",
    "This creates a new composite model that takes a [noise + control] vector as input, that then passes through the generator to produce an image. The image then passes through the discriminator model to produce a classification and through the auxiliary model to produce a prediction of the control variables.\n",
    "\n",
    "The model has two output layers that need to be trained with different loss functions. Binary cross entropy loss is used for the discriminator output, as we did when compiling the discriminator for standalone use, and mutual information loss is used for the auxiliary model, which, in this case, can be implemented directly as categorical cross-entropy and achieve the desired result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# define the combined discriminator, generator and q network model\n",
    "def define_gan(g_model, d_model, q_model):\n",
    "\t# make weights in the discriminator (some shared with the q model) as not trainable\n",
    "\td_model.trainable = False\n",
    "\t# connect g outputs to d inputs\n",
    "\td_output = d_model(g_model.output)\n",
    "\t# connect g outputs to q inputs\n",
    "\tq_output = q_model(g_model.output)\n",
    "\t# define composite model\n",
    "\tmodel = Model(g_model.input, [d_output, q_output])\n",
    "\t# compile model\n",
    "\topt = Adam(lr=0.0002, beta_1=0.5)\n",
    "\tmodel.compile(loss=['binary_crossentropy', 'categorical_crossentropy'], optimizer=opt)\n",
    "\treturn model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will develop inputs for the generator.\n",
    "\n",
    "Each input will be a vector comprised of noise and the control codes. Specifically, a vector of Gaussian random numbers and a one hot encoded randomly selected categorical value.\n",
    "\n",
    "The generate_latent_points() function below implements this, taking as input the size of the latent space, the number of categorical values, and the number of samples to generate as arguments. The function returns the input concatenated vectors as input for the generator model, as well as the standalone control codes. The standalone control codes will be required when updating the generator and auxiliary models via the composite GAN model, specifically for calculating the mutual information loss for the auxiliary model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate points in latent space as input for the generator\n",
    "def generate_latent_points(latent_dim, n_cat, n_samples):\n",
    "\t# generate points in the latent space\n",
    "\tz_latent = randn(latent_dim * n_samples)\n",
    "\t# reshape into a batch of inputs for the network\n",
    "\tz_latent = z_latent.reshape(n_samples, latent_dim)\n",
    "\t# generate categorical codes\n",
    "\tcat_codes = randint(0, n_cat, n_samples)\n",
    "\t# one hot encode\n",
    "\tcat_codes = to_categorical(cat_codes, num_classes=n_cat)\n",
    "\t# concatenate latent points and control codes\n",
    "\tz_input = hstack((z_latent, cat_codes))\n",
    "\treturn [z_input, cat_codes]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we can generate real and fake examples.\n",
    "\n",
    "The MNIST dataset can be loaded, transformed into 3D input by adding an additional dimension for the grayscale images, and scaling all pixel values to the range [-1,1] to match the output from the generator model. This is implemented in the load_real_samples() function below.\n",
    "\n",
    "We can retrieve batches of real samples required when training the discriminator by choosing a random subset of the dataset. This is implemented in the generate_real_samples() function below that returns the images and the class label of 1, to indicate to the discriminator that they are real images.\n",
    "\n",
    "The discriminator also requires batches of fake samples generated via the generator, using the vectors from generate_latent_points() function as input. The generate_fake_samples() function below implements this, returning the generated images along with the class label of 0, to indicate to the discriminator that they are fake images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# load images\n",
    "def load_real_samples():\n",
    "\t# load dataset\n",
    "\t(trainX, _), (_, _) = load_data()\n",
    "\t# expand to 3d, e.g. add channels\n",
    "\tX = expand_dims(trainX, axis=-1)\n",
    "\t# convert from ints to floats\n",
    "\tX = X.astype('float32')\n",
    "\t# scale from [0,255] to [-1,1]\n",
    "\tX = (X - 127.5) / 127.5\n",
    "\tprint(X.shape)\n",
    "\treturn X"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# select real samples\n",
    "def generate_real_samples(dataset, n_samples):\n",
    "\t# choose random instances\n",
    "\tix = randint(0, dataset.shape[0], n_samples)\n",
    "\t# select images and labels\n",
    "\tX = dataset[ix]\n",
    "\t# generate class labels\n",
    "\ty = ones((n_samples, 1))\n",
    "\treturn X, y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# use the generator to generate n fake examples, with class labels\n",
    "def generate_fake_samples(generator, latent_dim, n_cat, n_samples):\n",
    "\t# generate points in latent space and control codes\n",
    "\tz_input, _ = generate_latent_points(latent_dim, n_cat, n_samples)\n",
    "\t# predict outputs\n",
    "\timages = generator.predict(z_input)\n",
    "\t# create class labels\n",
    "\ty = zeros((n_samples, 1))\n",
    "\treturn images, y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need to keep track of the quality of the generated images.\n",
    "\n",
    "We will periodically use the generator to generate a sample of images and save the generator and composite models to file. We can then review the generated images at the end of training in order to choose a final generator model and load the model to start using it to generate images.\n",
    "\n",
    "The summarize_performance() function below implements this, first generating 100 images, scaling their pixel values back to the range [0,1], and saving them as a plot of images in a 10×10 square.\n",
    "\n",
    "The generator and composite GAN models are also saved to file, with a unique filename based on the training iteration number."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# generate samples and save as a plot and save the model\n",
    "def summarize_performance(step, g_model, gan_model, latent_dim, n_cat, n_samples=100):\n",
    "\t# prepare fake examples\n",
    "\tX, _ = generate_fake_samples(g_model, latent_dim, n_cat, n_samples)\n",
    "\t# scale from [-1,1] to [0,1]\n",
    "\tX = (X + 1) / 2.0\n",
    "\t# plot images\n",
    "\tfor i in range(100):\n",
    "\t\t# define subplot\n",
    "\t\tpyplot.subplot(10, 10, 1 + i)\n",
    "\t\t# turn off axis\n",
    "\t\tpyplot.axis('off')\n",
    "\t\t# plot raw pixel data\n",
    "\t\tpyplot.imshow(X[i, :, :, 0], cmap='gray_r')\n",
    "\t# save plot to file\n",
    "\tfilename1 = 'generated_plot_%04d.png' % (step+1)\n",
    "\tpyplot.savefig(filename1)\n",
    "\tpyplot.close()\n",
    "\t# save the generator model\n",
    "\tfilename2 = 'model_%04d.h5' % (step+1)\n",
    "\tg_model.save(filename2)\n",
    "\t# save the gan model\n",
    "\tfilename3 = 'gan_model_%04d.h5' % (step+1)\n",
    "\tgan_model.save(filename3)\n",
    "\tprint('>Saved: %s, %s, and %s' % (filename1, filename2, filename3))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can train the InfoGAN.\n",
    "\n",
    "This is implemented in the train() function below that takes the defined models and configuration as arguments and runs the training process.\n",
    "\n",
    "The models are trained for 100 epochs and 64 samples are used in each batch. There are 60,000 images in the MNIST training dataset, therefore one epoch involves 60,000/64, or 937 batches or training iterations. Multiplying this by the number of epochs, or 100, means that there will be a total of 93,700 total training iterations.\n",
    "\n",
    "Each training iteration involves first updating the discriminator with half a batch of real samples and half a batch of fake samples to form one batch worth of weight updates, or 64, each iteration. Next, the composite GAN model is updated based on a batch worth of noise and control code inputs. The loss of the discriminator on real and fake images and the loss of the generator and auxiliary model is reported each training iteration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# train the generator and discriminator\n",
    "def train(g_model, d_model, gan_model, dataset, latent_dim, n_cat, n_epochs=100, n_batch=64):\n",
    "\t# calculate the number of batches per training epoch\n",
    "\tbat_per_epo = int(dataset.shape[0] / n_batch)\n",
    "\t# calculate the number of training iterations\n",
    "\tn_steps = bat_per_epo * n_epochs\n",
    "\t# calculate the size of half a batch of samples\n",
    "\thalf_batch = int(n_batch / 2)\n",
    "\t# manually enumerate epochs\n",
    "\tfor i in range(n_steps):\n",
    "\t\t# get randomly selected 'real' samples\n",
    "\t\tX_real, y_real = generate_real_samples(dataset, half_batch)\n",
    "\t\t# update discriminator and q model weights\n",
    "\t\td_loss1 = d_model.train_on_batch(X_real, y_real)\n",
    "\t\t# generate 'fake' examples\n",
    "\t\tX_fake, y_fake = generate_fake_samples(g_model, latent_dim, n_cat, half_batch)\n",
    "\t\t# update discriminator model weights\n",
    "\t\td_loss2 = d_model.train_on_batch(X_fake, y_fake)\n",
    "\t\t# prepare points in latent space as input for the generator\n",
    "\t\tz_input, cat_codes = generate_latent_points(latent_dim, n_cat, n_batch)\n",
    "\t\t# create inverted labels for the fake samples\n",
    "\t\ty_gan = ones((n_batch, 1))\n",
    "\t\t# update the g via the d and q error\n",
    "\t\t_,g_1,g_2 = gan_model.train_on_batch(z_input, [y_gan, cat_codes])\n",
    "\t\t# summarize loss on this batch\n",
    "\t\tprint('>%d, d[%.3f,%.3f], g[%.3f] q[%.3f]' % (i+1, d_loss1, d_loss2, g_1, g_2))\n",
    "\t\t# evaluate the model performance every 'epoch'\n",
    "\t\tif (i+1) % (bat_per_epo * 10) == 0:\n",
    "\t\t\tsummarize_performance(i, g_model, gan_model, latent_dim, n_cat)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then configure and create the models, then run the training process.\n",
    "\n",
    "We will use 10 values for the single categorical variable to match the 10 known classes in the MNIST dataset. We will use a latent space with 64 dimensions to match the InfoGAN paper, meaning, in this case, each input vector to the generator model will be 64 (random Gaussian variables) + 10 (one hot encoded control variable) or 72 elements in length."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# number of values for the categorical control code\n",
    "n_cat = 10\n",
    "# size of the latent space\n",
    "latent_dim = 62\n",
    "# create the discriminator\n",
    "d_model, q_model = define_discriminator(n_cat)\n",
    "# create the generator\n",
    "gen_input_size = latent_dim + n_cat\n",
    "g_model = define_generator(gen_input_size)\n",
    "# create the gan\n",
    "gan_model = define_gan(g_model, d_model, q_model)\n",
    "# load image data\n",
    "dataset = load_real_samples()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we train our model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# train model\n",
    "train(g_model, d_model, gan_model, dataset, latent_dim, n_cat)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
